﻿using UnityEngine;
using System.Collections;

public class Players : MonoBehaviour {
  [SerializeField]
  private int playerCount = 1;
  [SerializeField]
  private int maxPlayerCount = 2;
	// Use this for initialization
	void Start () {
    if (playerCount == 1)
    {
      GameObject Player = GameObject.FindGameObjectWithTag("Player1");
      Player.GetComponentInChildren<Camera>().enabled = false;
      for (int i = 2; i <= maxPlayerCount; i++)
      {
        string name = "Player" + maxPlayerCount.ToString();
        Player = GameObject.FindGameObjectWithTag(name);
        Player.SetActive(false);
      }
    }
    else
    {
      Camera.main.enabled = false;

    }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
