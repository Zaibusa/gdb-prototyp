﻿using UnityEngine;
using System.Collections;

// Isometric Camera Controller
// Rotate camera around player with 'y' and 'x' keys.
// The camera will rotate exactly in 90° turns, which
// results in four different views.
// Zoom in and out with '+' and '-' keys.
// The camera will zoom in or out by the factor 15.

public class IsoCamController : MonoBehaviour {

	public GameObject Player;				// The player object
	public float OrthoSizeMin = 30.0f;		// Min zoom in factor
	public float OrthoSizeMax = 90.0f;		// Max zoom out factor

	float RotYStep = 45.0f;					// Initial Y rotation step size
	float OrthoSizeStep = 15.0f;			// Orthographic camera zoom step size
											
	float dtInit = 0.2f;					// Time which needs to pass, before another key input will be processed
	float dt = 0.0f;						// Regular delta time
	float dtRot = 0.0f;						// delta time for rotation
	float dtZoom = 0.0f;					// delta time for zoom


	// Use this for initialization
	void Start () {
		// Set initial camera position
		// Car should be in the center of this projection
		// Set tag to "Player1" if not specified
		Player = GameObject.FindGameObjectWithTag ("Player1");
		transform.position = Player.transform.position;

		// Set the initial camera rotation
		// For isometric view use X=30 Y=45 Z=0
		transform.rotation = Quaternion.Euler(50, RotYStep, 0);

		// Set the initial othorgaphic size e.g. zoom level
		camera.orthographicSize = 45.0f;
	}
	
	// Update is called once per frame
	void Update () {
		dt = Time.deltaTime;				// Get delta time
		dtRot -= dt;
		dtZoom -= dt;
		transform.position = Player.transform.position;

		// Rotation
		if (Input.GetKey ("x")) {
			// Rotate clockwise around the player
			if( dtRot < 0.0f) {
				RotYStep -= 90.0f;
				transform.rotation = Quaternion.Euler(30, RotYStep, 0);
				dtRot = dtInit;
			}
		}
		if (Input.GetKey ("y")) {
			// Rotate counter-clockwise around the player
			if( dtRot < 0.0f) {
				RotYStep += 90.0f;
				transform.rotation = Quaternion.Euler(30, RotYStep, 0);
				dtRot = dtInit;
			}
		}

		// Zoom
		if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.Equals) || Input.GetKey(KeyCode.KeypadPlus)) {
			// Zoom in to the player
			// The english and german keyboard layout differ, thus we check
			// for KeyCode.Plus (ENG) and KeyCode.Equals (GER). For the
			// plus on the numpad we use KeyCode.KeypadPlus.
			if( dtZoom < 0.0f && camera.orthographicSize > OrthoSizeMin ) {
				camera.orthographicSize -= OrthoSizeStep;
				dtZoom = dtInit;
			}
		}
		if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus)) {
			// Zoom out of the player
			if( dtZoom < 0.0f && camera.orthographicSize < OrthoSizeMax ) {
				camera.orthographicSize += OrthoSizeStep;
				dtZoom = dtInit;
			}
		}
	}
}
