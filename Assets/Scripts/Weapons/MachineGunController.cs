﻿using UnityEngine;
using System.Collections;

public class MachineGunController : MonoBehaviour {

	public Rigidbody RigidBodyBullet;
	public float BulletSpeed = 100;
	public bool doubleBullet = true;
	public float bulletOffsetFront = 5.0f;
	public float bulletOffsetSide = 1.5f;
    private AudioSource MachineGunAudioSource;
    public AudioClip BulletSound;

	Vector3 posInFrontOfObject;

	float dtInit = 0.1f;
	float dt = 0.0f;

	// Use this for initialization
	void Start () {
		dt = dtInit;
    MachineGunAudioSource = this.gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		dt -= Time.deltaTime;

		if (Input.GetButton ("Fire1")) {
			if( dt < 0.0f) {
        MachineGunAudioSource.PlayOneShot(BulletSound, 1.0f);
				// Fire bullet each time step
				if( doubleBullet ) {
					FireDoubleBullets();
				}
				else{
					FireSingleBullet();
				}
				dt = dtInit;
			}
		}
	}

	/*
	 * FireSingleBullet() methode
	 * Fires one bullet in front of the car
	 */
	void FireSingleBullet() {
		//Get current car object position and place bullet in front of it
		posInFrontOfObject = transform.position + transform.forward * 5.0f;

		// Instantiate bullet
		Rigidbody bulletClone = (Rigidbody)Instantiate (RigidBodyBullet, posInFrontOfObject, transform.rotation);
		bulletClone.velocity = (transform.forward + new Vector3(0, 0.1f, 0)) * BulletSpeed;

		// Make object instance destroyable
		bulletClone.GetComponent<BulletCollision> ().destroyable = true;
	}

	/*
	 * FireDoubleBullets() methode
	 * Fires two bullets in front of the car
	 */
	void FireDoubleBullets() {
		//Get current car object position and place bullet spaced in front of it

		// Instantiate right bullet
		posInFrontOfObject = transform.position + (transform.right * 1.5f) + (transform.forward * bulletOffsetFront);
		Rigidbody leftBulletClone = (Rigidbody)Instantiate (RigidBodyBullet, posInFrontOfObject, transform.rotation);
        leftBulletClone.velocity = (transform.forward + new Vector3(0, 0.1f, 0)) * BulletSpeed;
		leftBulletClone.GetComponent<BulletCollision> ().destroyable = true;

		// Instantiate left bullet
		posInFrontOfObject = transform.position - (transform.right * 1.5f) + (transform.forward * bulletOffsetFront);
		Rigidbody rightBulletClone = (Rigidbody)Instantiate (RigidBodyBullet, posInFrontOfObject, transform.rotation);
        rightBulletClone.velocity = (transform.forward + new Vector3(0, 0.1f, 0)) * BulletSpeed;
		rightBulletClone.GetComponent<BulletCollision> ().destroyable = true;
	}
}
