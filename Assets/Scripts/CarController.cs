﻿using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour
{

    //CarController1.js
    public Transform[] wheels;
    [SerializeField]
    float enginePower = 40.0f;
    [SerializeField]
    float fuel = 100.0f;
    [SerializeField]
    float power = 0.0f;
    [SerializeField]
    float brake = 0.0f;
    [SerializeField]
    float steer = 0.0f;
    [SerializeField]
    float maxSteer = 25.0f;
    
    public string controllPower = "Horizontal";
    public string controllSteer = "Vertical";
    public string controllBreak = "space";
    public AudioClip idleSound;
    public AudioClip engineSound;
    //AnimationClip clip = new AnimationClip();
    Vector3 frontDownforcePoint;
    Vector3 rearDownforcePoint;
    Vector3 frontRightDownforcePoint;
    Vector3 frontLeftDownforcePoint;
    Vector3 rearRightDownforcePoint;
    Vector3 rearLeftDownforcePoint;
    AnimationCurve frontDownforceCurve;
    AnimationCurve rearDownforceCurve;
    public bool downforce = true;
    public float mass = 2000;
    private bool useFuel = true;
	//Sound Pitch
	float startPitch = 0.4f;

    void Start()
    {
        rigidbody.centerOfMass = new Vector3(0f, -1.5f, 0.0f);
        frontDownforceCurve = new AnimationCurve(
          new Keyframe(0, 0, 0, 0),
          new Keyframe(1, 0.8298343f, -0.03606727f, -0.03606727f));
        rearDownforceCurve = new AnimationCurve(
          new Keyframe(0, 0, 0, 0),
          new Keyframe(1, 0.8298343f, -0.03606727f, -0.03606727f));
        // downforce
        Bounds totalBounds = GetBounds(this.gameObject);
        frontDownforcePoint = Vector3.Scale(totalBounds.max, new Vector3(0, 0, 1)) + Vector3.up;
        rearDownforcePoint = Vector3.Scale(frontDownforcePoint, new Vector3(0, 1, -1));

    
		audio.pitch = startPitch;
	}

        //frontRightDownforcePoint = GetCollider(0).transform.position;
        //frontLeftDownforcePoint = GetCollider(1).transform.position;
        //rearRightDownforcePoint = GetCollider(3).transform.position;
        //rearLeftDownforcePoint = GetCollider(2).transform.position;




    void Update()
    {

      if (Input.GetKeyDown("r"))
      {
        this.gameObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
        this.gameObject.transform.rotation = new Quaternion(0.0f, this.transform.rotation.y, 0.0f, 0.0f);
        fuel -= 10;

      }
      if (Input.GetKeyUp("f"))
          useFuel = !useFuel;
      float test = 0.0f;
        if (fuel > 0.0)
        {
          test = Input.GetAxis(controllSteer);
        }
            power = test * enginePower * Time.deltaTime * 250.0f;
            brake = Input.GetKey(controllBreak) ? rigidbody.mass * 0.1f : 0.0f;
            if (useFuel == true)
                fuel -= power * 0.00005f;
        
        steer = Input.GetAxis(controllPower) * maxSteer;
        GetCollider(0).steerAngle = steer;
        GetCollider(1).steerAngle = steer;

		if (power == 0 && audio.pitch > 0.4f)
		{
			audio.pitch -= audio.pitch * 0.01f;
		}
		else if(power > 0 && audio.pitch <= 1.0f)
		{
			audio.pitch += startPitch*0.01f;
		} 

        if (brake > 0.0)
        {

            GetCollider(0).brakeTorque = brake;

            GetCollider(1).brakeTorque = brake;

            GetCollider(2).brakeTorque = brake;

            GetCollider(3).brakeTorque = brake;

            GetCollider(2).motorTorque = 0.0f;

            GetCollider(3).motorTorque = 0.0f;

        }
        else
        {

            GetCollider(0).brakeTorque = 0;

            GetCollider(1).brakeTorque = 0;

            GetCollider(2).brakeTorque = 0;

            GetCollider(3).brakeTorque = 0;

            GetCollider(2).motorTorque = power;

            GetCollider(3).motorTorque = power;

        }

    }

    void FixedUpdate()
    {
      if (downforce)
      {
          //rigidbody.AddForceAtPosition(
          //    -transform.up * rigidbody.mass * frontDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
          //    transform.TransformPoint(frontDownforcePoint));
          //rigidbody.AddForceAtPosition(
          //    -transform.up * rigidbody.mass * rearDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
          //    transform.TransformPoint(rearDownforcePoint));

          rigidbody.AddForceAtPosition(
              -transform.up * rigidbody.mass * frontDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
              transform.TransformPoint(frontRightDownforcePoint));
          rigidbody.AddForceAtPosition(
              -transform.up * rigidbody.mass * frontDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
              transform.TransformPoint(frontLeftDownforcePoint));
          rigidbody.AddForceAtPosition(
              -transform.up * rigidbody.mass * rearDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
              transform.TransformPoint(rearRightDownforcePoint));
          rigidbody.AddForceAtPosition(
              -transform.up * rigidbody.mass * rearDownforceCurve.Evaluate(rigidbody.velocity.magnitude / 100),
              transform.TransformPoint(rearLeftDownforcePoint));
      }
    }

    public void UpdateFuel(float value)
    {
        fuel += value;
    }

	public float GetFuel()
	{
		return fuel;		
	}

    WheelCollider GetCollider(int n)
    {
        return wheels[n].gameObject.GetComponent<WheelCollider>();
    }

    Bounds GetBounds(GameObject obj) {
	    var totalBounds = obj.GetComponentInChildren<Collider>().bounds;
	    var colliders = obj.GetComponentsInChildren<Collider>();
	    foreach (Collider col in colliders) totalBounds.Encapsulate(col.bounds);
	return totalBounds;
}
}
