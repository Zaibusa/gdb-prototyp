﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour
{
	[SerializeField]
	public float health = 25.0f;
    private GameObject go;

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Projectile" || collision.gameObject.tag == "MachineGunBullet")
		{
			health -= 5.0f;

			if (health <= 0.0f)
			{
				//Destroy(gameObject);
                gameObject.SetActive(false);
                if (gameObject.layer == 9) // Enemy Layer
                {
                    go = Instantiate(Resources.Load("Fuel")) as GameObject;
                    go.transform.position = transform.position - new Vector3(0, 2, 0);
                }
			}
		}
	}

	public float GetHealth()
	{
		return health;
	}
}
