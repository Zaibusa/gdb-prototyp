﻿using UnityEngine;
using System.Collections;

public class BulletCollision : MonoBehaviour {

	public bool destroyable = false;					// Use this to set an initial bullet prefab to exist FOREVER!

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// OnCollisionEnter method
	void OnCollisionEnter (Collision collision) {
		
		if (destroyable)
		{
			Destroy (gameObject);
		}

		// Further functionalities
		//if (collision.gameObject.name == "EnemyTarget") {
			// Drain Enemy HP
		//}
	}
}
