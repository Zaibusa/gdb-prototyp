﻿using UnityEngine;
using System.Collections;

public class SimpleGUI : MonoBehaviour {

	GameObject car;

	// Use this for initialization
	void Start () {
		car = GameObject.FindGameObjectWithTag("Player1");
	}

	void OnGUI()
	{
        GUI.Box(new Rect(40, 5, 280, 50), "");
        string health = "";
        string fuel = "";
        for (int i = 0; i < car.GetComponent<Damage>().GetHealth() / 4; i++)
        {
            health += "\u258B";
        }

        for (int i = 0; i < car.GetComponent<CarController>().GetFuel() / 10; i++)
        {
            fuel += "\u258B";
        }

        GUI.Label(new Rect(50, 10, 350, 200), "Health: " + health);
		GUI.Label(new Rect(50, 30, 350, 200), "  Fuel:  " + fuel);
	}
	// Update is called once per frame
	void Update () {
	
	}
}
