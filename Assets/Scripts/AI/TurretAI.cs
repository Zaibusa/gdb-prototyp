﻿using UnityEngine;
using System.Collections;

public class TurretAI : MonoBehaviour
{
	[SerializeField]
	private GameObject shotPrefab;
	[SerializeField]
	private GameObject launcher;
	[SerializeField]
	private float shotSpeed = 100.0f;
	[SerializeField]
	private float fireRate = 3.0f;
	private GameObject player;
	private bool attacking = false;
	private Transform target;
	private float timer = 0.0f;

	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindWithTag("Player1");
		timer = fireRate;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;

		if (attacking && timer >= fireRate)
		{
			target = player.transform;
			GameObject shot = Instantiate(shotPrefab, launcher.transform.position, launcher.transform.rotation) as GameObject;
			shot.SetActive(true);
			Vector3 velocity = (target.position - shot.transform.position);
			shot.rigidbody.velocity = velocity.normalized * shotSpeed;
			timer = 0.0f;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player1")
			attacking = true;
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player1")
			attacking = false;
	}
}
