﻿using UnityEngine;
using System.Collections;

public class fuel : MonoBehaviour {
    public int capacity;
	// Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        //int TargetPlayer = 0;
        if (other.gameObject.layer == 8)
        {
            this.gameObject.SetActive(false);
            other.gameObject.GetComponent<CarController>().UpdateFuel(capacity);
        }
    }
}
