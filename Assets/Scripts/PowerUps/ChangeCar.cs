﻿using UnityEngine;
using System.Collections;

public class ChangeCar : MonoBehaviour {

    public AudioClip Bulletsound;
    public Rigidbody RigidBodyBullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    void OnTriggerEnter(Collider other)
    {
        //int TargetPlayer = 0;
        if (other.gameObject.layer == 8)
        {
            this.gameObject.SetActive(false);
            other.gameObject.GetComponent<Damage>().health = 100;
            other.gameObject.renderer.material.SetColor("_Color", new Color(0,0,255,0));
            other.gameObject.AddComponent<MachineGunController>();
            other.gameObject.GetComponent<MachineGunController>().BulletSound = Bulletsound;
            other.gameObject.GetComponent<MachineGunController>().RigidBodyBullet = RigidBodyBullet;
        }
    }
}
